import React from 'react';
import logo from './assets/img/logo.svg';
import './assets/css/App.css';

//Importar componente
import MiComponente from './components/MiComponente'

function HolaMundo(nombre){
  var text = "hola "+ nombre
  return text
}

function App() {

  var nombre ="cristian"

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
  
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        {
          HolaMundo(nombre)
        }
      <section className ="componentes">
        <MiComponente></MiComponente>
      </section>

      </header>

    </div>
  );
}

export default App;
