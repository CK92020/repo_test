import React , {Component} from 'react'

class MiComponente extends Component{
    render(){

        let receta =  {
            nombre:'pizza',
            color: 'rojo',
            tipos: ['negro','azul','verde']
        }
        return(
            <React.Fragment>
            <h1>Hola Mundo ZAP</h1>
            <h1>Como estas? {receta.nombre}</h1>
            
            {
                receta.tipos.map((tipo,i) =>{
                    console.log(tipo);
                return <li key={i}>{tipo}</li>
                })
            }
            </React.Fragment>
        );
    }
}

export default MiComponente;